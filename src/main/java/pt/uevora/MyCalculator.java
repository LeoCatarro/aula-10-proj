package pt.uevora;

import java.util.Scanner;

public class MyCalculator {

    public Double execute(String expression)
    {
        //DIVISÃO
        if(expression.contains("/"))
        {
            String[] split = expression.split("\\/");

            if(split[1] == "0")
            {
                throw new IllegalArgumentException("Cant do: n / 0");
            }
            return div(split[0], split[1]);
        }

        //MULTIPLICAÇÃO
        if(expression.contains("*"))
        {
            String[] split = expression.split("\\*");
            return mult(split[0],split[1]);
        }

        //POTENCIAÇÃO
        if(expression.contains("^"))
        {
            String[] split = expression.split("\\^");
            return pot(split[0],split[1]);
        }

        //SOMA
        if(expression.contains("+"))
        {
            String[] split = expression.split("\\+");
            return sum(split[0],split[1]);
        }

        //SUBTRAÇÃO
        if(expression.contains("-"))
        {
            int count=0;
            for(int i=0 ; i<expression.length() ; i++)
            {
                if(expression.charAt(i) == '-')
                {
                    count++;
                }
            }

            //Subs do tipo: x - y
            if(count==1)
            {
                String[] split = expression.split("\\-");
                return sub(split[0],split[1]);
            }

            //Subs do tipo: -x - y || x - (-y)
            if(count==2)
            {
                String[] split = expression.split("\\-");
                if(expression.charAt(0) == '-')
                {
                    return -sum(split[1],split[2]);
                }
                return sum(split[0],split[2]);
            }

            //Subs do tipo: -x - (-y)
            if(count==3)
            {
                String[] split = expression.split("\\-");
                return sub(split[3],split[1]);
            }
        }




        throw new IllegalArgumentException("Invalid expression!");
    }

    private Double sum(String arg1, String arg2) { return new Double(arg1) + new Double(arg2); }

    private Double sub(String arg1, String arg2) { return new Double(arg1) - new Double(arg2); }

    private Double mult(String arg1, String arg2) { return new Double(arg1) * new Double(arg2); }

    private Double div(String arg1, String arg2) { return new Double(arg1) / new Double(arg2); }

    private Double pot(String arg1, String arg2) { return new Double(Math.pow(new Double(arg1), new Double(arg2))); }

    public static void main(String[] args) {
        while(true)
        {
            System.out.println("Calculator");
            System.out.println("Enter your expression:");
            Scanner scanner = new Scanner(System.in);
            String expression = scanner.nextLine();

            MyCalculator myCalculator = new MyCalculator();
            Object result = myCalculator.execute(expression);

            System.out.println("Result :  " + result);
        }
    }

}
