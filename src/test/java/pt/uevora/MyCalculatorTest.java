package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    /***********************************************
            Testes para inserção de input diferente
     ***********************************************/

    @Test(expected = NumberFormatException.class)
    public void invalidInputInsertSum() throws Exception {
        Double result = calculator.execute("a + b");

        fail("Invalid Input Format!");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidInputInsert2Sum() throws Exception {
        Double result = calculator.execute("! + ?");

        fail("Invalid Input Format!");
    }


    @Test(expected = NumberFormatException.class)
    public void invalidInputInsertSub() throws Exception {
        Double result = calculator.execute("a - b");

        fail("Invalid Input Format!");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidInputInsert2Sub() throws Exception {
        Double result = calculator.execute("! - ?");

        fail("Invalid Input Format!");
    }


    @Test(expected = NumberFormatException.class)
    public void invalidInputInsertMult() throws Exception {
        Double result = calculator.execute("a * b");

        fail("Invalid Input Format!");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidInputInsert2Mult() throws Exception {
        Double result = calculator.execute("! * ?");

        fail("Invalid Input Format!");
    }


    @Test(expected = NumberFormatException.class)
    public void invalidInputInsertDiv() throws Exception {
        Double result = calculator.execute("a / b");

        fail("Invalid Input Format!");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidInputInsert2Div() throws Exception {
        Double result = calculator.execute("! / ?");

        fail("Invalid Input Format!");
    }


    @Test(expected = NumberFormatException.class)
    public void invalidInputInsertPot() throws Exception {
        Double result = calculator.execute("a ^ b");

        fail("Invalid Input Format!");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidInputInsert2Pot() throws Exception {
        Double result = calculator.execute("! ^ ?");

        fail("Invalid Input Format!");
    }
    /*******************************
            Testes para a Soma
    *******************************/

    @Test
    public void sumBewtweenPositiveNumbers() throws Exception {
        Double result = calculator.execute("2 + 3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }

    @Test
    public void sumBetweenNegativeNumbersShouldReturnNegativeNumber() throws Exception
    {
        Double result = calculator.execute("-2 + -1");

        assertEquals("The sum result of -1 + -1 must be -3",  -3D, (Object)result);
    }

    @Test
    public void sumBetweenPositiveNumberAndZeroShouldReturnThePositiveNumber() throws Exception
    {
        Double result = calculator.execute("10 + 0");

        assertEquals("The sum result of 10 + 0 must be 10",  10D, (Object)result);
    }

    @Test
    public void sumBetweenNegativeNumberAndZeroShouldReturnTheNegativeNumber() throws Exception
    {
        Double result = calculator.execute("-1 + 0");

        assertEquals("The sum result of -1 + 0 must be -1",  -1D, (Object)result);
    }

    @Test
    public void sumBetweenPositiveAndNegativeNumber() throws Exception
    {
        Double result = calculator.execute(" 5 + -3");

        assertEquals("The sum result of 5 + -3 must be 2",  2D, (Object)result);
    }

    /***********************************
            Testes para a Subtração
     **********************************/

    @Test
    public void subBetweenPositiveNumbers() throws Exception
    {
        Double result = calculator.execute("2 - 3");

        assertEquals("The sub result of 2 - 3 must be -1",  -1D, (Object)result);
    }

    @Test
    public void subNegativeNumbers() throws Exception
    {
        Double result = calculator.execute("-2 - -6");

        assertEquals("The sub result of -2 - -6 must be 4",  4D, (Object)result);
    }

    @Test
    public void subBetweenNegativeNumberAndZeroShouldReturnTheNegativeNumber() throws Exception
    {
        Double result = calculator.execute(" -3 - 0");

        assertEquals("The sub result of -3 - 0 must be -3",  -3D, (Object)result);
    }

    @Test
    public void subBetweenZeroAndNegativeNumberShouldReturnTheModuleOfNumber() throws Exception
    {
        Double result = calculator.execute(" 0 - -3");

        assertEquals("The sub result of 0 - -3 must be 3",  3D, (Object)result);
    }

    @Test
    public void subBetweenZeroesShouldReturnZero() throws Exception
    {
        Double result = calculator.execute(" 0 - 0");

        assertEquals("The sub result of 0 - 0 must be 0",  0D, (Object)result);
    }

    /****************************************
            Testes para a Multiplicação
     ****************************************/

    @Test
    public void multBetweenZeroes() throws Exception
    {
        Double result = calculator.execute(" 0 * 0");

        assertEquals("The multiplication result of 0 * 0 must be 0",  0D, (Object)result);
    }

    @Test
    public void multBetweenPositiveNumbers() throws Exception
    {
        Double result = calculator.execute(" 2 * 4");

        assertEquals("The multiplication result of 2 * 4 must be 8",  8D, (Object)result);
    }

    @Test
    public void multBetweenNegativeNumbers() throws Exception
    {
        Double result = calculator.execute(" -2 * -4");

        assertEquals("The multiplication result of -2 * -4 must be 8",  8D, (Object)result);
    }

    @Test
    public void multBetweenPositiveAndNegativeNumbers() throws Exception
    {
        Double result = calculator.execute(" -2 * 4");

        assertEquals("The multiplication result of -2 * 4 must be -8",  -8D, (Object)result);
    }

    /****************************************
            Testes para a Divisão
     ****************************************/

    @Test
    public void divBetweenPositiveNumbers() throws Exception
    {
        Double result = calculator.execute(" 2 / 4 ");

        assertEquals("The division result of 2 / 4 must be 0.5",  0.5D, (Object)result);
    }

    @Test
    public void divBetweenNegativeNumbers() throws Exception
    {
        Double result = calculator.execute(" -4 / -2 ");

        assertEquals("The division result of -4 / -2 must be 2",  2D, (Object)result);
    }

    @Test
    public void divBetweenPositiveAndNegativeNumbers() throws Exception
    {
        Double result = calculator.execute(" 4 / -2 ");

        assertEquals("The division result of -4 / 2 must be -2",  -2D, (Object)result);
    }

    @Test
    public void divWithZeroAsNumerator() throws Exception
    {
        Double result = calculator.execute(" 0 / 8 ");

        assertEquals("The division result of 0 / 8 must be 0",  0D, (Object)result);
    }

    @Test(expected = ArithmeticException.class)
    public void divWithZeroAsDenominator() throws Exception
    {
        Double result = calculator.execute(" 8 / 0 ");
    }


    /****************************************
            Testes para a Potenciação
     ****************************************/

    @Test
    public void PotWithPositiveNumbers() throws Exception
    {
        Double result = calculator.execute(" 2^2 ");

        assertEquals("The division result of 2 ^ 2 must be 4",  4D, (Object)result);
    }

    @Test
    public void PotWithNegativeNumberInBase() throws Exception
    {
        Double result = calculator.execute(" -2^3 ");

        assertEquals("The division result of -2 ^ 3 must be -8",  -8D, (Object)result);
    }

    @Test
    public void PotWithNegativeNumberInBase2() throws Exception
    {
        Double result = calculator.execute(" -2^2 ");

        assertEquals("The division result of -2 ^ 2 must be 4",  4D, (Object)result);
    }

    @Test
    public void PotWithNegativeNumberInExponent() throws Exception
    {
        Double result = calculator.execute(" 2^-2 ");

        assertEquals("The division result of 2 ^ -2 must be 0.25",  0.25D, (Object)result);
    }

    @Test
    public void PotWithZeroInBase() throws Exception
    {
        Double result = calculator.execute(" 0^2 ");

        assertEquals("The division result of 0 ^ 2 must be 0",  0D, (Object)result);
    }

    @Test
    public void PotWithZeroInExponent() throws Exception
    {
        Double result = calculator.execute(" 2^0 ");

        assertEquals("The division result of 2^0 must be 1",  1D, (Object)result);
    }
}